// Simple rotation
/*
const blueBox = new Entity()
const blueBoxShape = new GLTFShape("models/BlueBox.glb")
blueBox.addComponent (blueBoxShape)
blueBox.addComponent(new Transform({ position: new Vector3(8, 5, 8) }))
engine.addEntity(blueBox)

class Rotation {
  update(dt: number) {  
    const transform = blueBox.getComponent(Transform)  
    transform.rotate(Vector3.Up(), dt * 100) 
  }
}
engine.addSystem(new Rotation)

*/

// Pivot rotation

const blueBox = new Entity()
blueBox.addComponent(new GLTFShape("models/BlueBox.glb"))

const pivot = new Entity()
pivot.addComponent(new Transform({position: new Vector3(8, 2, 8)}))
engine.addEntity(pivot)

blueBox.addComponent(new Transform({ position: new Vector3(3, 2, 3)}))
blueBox.setParent(pivot)
engine.addEntity(blueBox)

class Rotation {
  update() {  
    const transform = pivot.getComponent(Transform)  
    transform.rotate(Vector3.Up(), 3) 
  }
}
engine.addSystem(new Rotation)











/*
/// --- Set up a system ---

class RotatorSystem {
  // this group will contain every entity that has a Transform component
  group = engine.getComponentGroup(Transform)

  update(dt: number) {
    // iterate over the entities of the group
    for (let entity of this.group.entities) {
      // get the Transform component of the entity
      const transform = entity.getComponent(Transform)

      // mutate the rotation
      transform.rotate(Vector3.Up(), dt * 10)
    }
  }
}

// Add a new instance of the system to the engine
engine.addSystem(new RotatorSystem())

/// --- Spawner function ---

function spawnCube(x: number, y: number, z: number) {
  // create the entity
  const cube = new Entity()

  // add a transform to the entity
  cube.addComponent(new Transform({ position: new Vector3(x, y, z) }))

  // add a shape to the entity
  cube.addComponent(new BoxShape())

  // add the entity to the engine
  engine.addEntity(cube)

  return cube
}

/// --- Spawn a cube ---

const cube = spawnCube(8, 1, 8)

cube.addComponent(
  new OnClick(() => {
    cube.getComponent(Transform).scale.z *= 1.1
    cube.getComponent(Transform).scale.x *= 0.9

    spawnCube(Math.random() * 8 + 1, Math.random() * 8, Math.random() * 8 + 1)
  })
)
*/